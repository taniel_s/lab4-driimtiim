package ee.ut.math.tvt.salessystem.ui.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;

public class TeamController implements Initializable {

    @FXML
    private Text teamname;
    @FXML
    private Text leaderName;
    @FXML
    private Text leaderEmail;
    @FXML
    private Text members;
    @FXML
    private ImageView imageView;

    private static final Logger log = LogManager.getLogger(TeamController.class);

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        log.info("Team Controller initialized");
        String dir = System.getProperty("user.dir");
        dir = dir.replace("SaleSystemGUI","src\\main\\resources\\application.properties");
        try (InputStream input = new FileInputStream(dir)) {

            Properties prop = new Properties();
            prop.load(input);

            teamname.setText(prop.getProperty("teamName"));
            leaderName.setText(prop.getProperty("teamLeader"));
            leaderEmail.setText(prop.getProperty("teamLeaderEmail"));
            members.setText(prop.getProperty("teamMembers"));
            imageView.setImage(new Image(prop.getProperty("imageLoc")));

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
