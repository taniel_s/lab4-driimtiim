package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.Warehouse;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class StockController implements Initializable {

    private static final Logger log = LogManager.getLogger(StockController.class);
    private final SalesSystemDAO dao;
    private final Warehouse warehouse;

    @FXML
    private Button addItem;
    @FXML
    private Button deleteItem;
    @FXML
    private Button refreshWarehouse;
    @FXML
    private TableView<StockItem> warehouseTableView;
    @FXML
    private TextField barCodeField;
    @FXML
    private TextField quantityField;
    @FXML
    private TextField nameField;
    @FXML
    private TextField priceField;


    public StockController(SalesSystemDAO dao, Warehouse warehouse) {
        this.dao = dao;
        this.warehouse = warehouse;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        refreshStockItems();
    }

    @FXML
    public void refreshButtonClicked() {
        refreshStockItems();
    }

    protected void refreshStockItems() {
        List<StockItem> items = dao.findStockItems();
        if (items != null) {
            warehouseTableView.setItems(FXCollections.observableList(items));
            warehouseTableView.refresh();
        }
    }

    /**
     * Event handler for the <code>addItem</code> event.
     */
    @FXML
    protected void addItemButtonClicked() {
        try {
            warehouse.createItemAndAddToWarehouse(barCodeField.getText(), nameField.getText(), priceField.getText(), quantityField.getText());
            refreshStockItems();
        } catch (SalesSystemException sse) {
            log.debug("Invalid input: " + sse.getMessage());
            AlertBox.display("Invalid input", "Could not add item to warehouse.\n" + sse.getMessage());
        } finally {
            emptyInputFields();
        }
    }

    protected void emptyInputFields() {
        barCodeField.setText("");
        quantityField.setText("");
        nameField.setText("");
        priceField.setText("");
    }

    /**
     * Event handler for the <code>deleteItem</code> event.
     */
    @FXML
    protected void deleteItemButtonClicked() {
        StockItem selectedItem = warehouseTableView.getSelectionModel().getSelectedItem();

        if (selectedItem != null) {
            dao.beginTransaction();
            dao.removeStockItem(selectedItem);
            dao.commitTransaction();
            refreshStockItems();
        } else {
            AlertBox.display("Item not selected", "Please select an item for deletion.");
        }
    }
}
