package ee.ut.math.tvt.salessystem.ui.controllers;

import javafx.scene.control.Alert;

public class AlertBox {
    public static void display(String problem, String message) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Error");
        alert.setHeaderText(problem);
        alert.setContentText(message);
        alert.showAndWait();
    }
}
