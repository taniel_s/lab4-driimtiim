package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Encapsulates everything that has to do with the purchase tab (the tab
 * labelled "History" in the menu).
 */
public class HistoryController implements Initializable {

    private static final Logger log = LogManager.getLogger(HistoryController.class);
    private final SalesSystemDAO dao;

    @FXML
    private Button showBetweenButton;
    @FXML
    private Button showLast10Button;
    @FXML
    private Button showAllButton;
    @FXML
    private TableView<Purchase> historyTableView;
    @FXML
    private TableView<SoldItem> singlePurchaseTableView;
    @FXML
    private DatePicker startDate;
    @FXML
    private DatePicker endDate;

    public HistoryController(SalesSystemDAO dao) {
        this.dao = dao;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        log.info("History controller initialized");

        historyTableView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection == null) {
                return;
            }
            singlePurchaseTableView.setItems(FXCollections.observableList(newSelection.getItems()));
        });
    }

    /**
     * Event handler for the <code>showAllButton</code> event.
     */
    @FXML
    protected void showAllButtonClicked() {
        List<Purchase> purchases = dao.findPurchases();
        log.debug("Showing all " + purchases.size() + " purchases.");
        historyTableView.setItems(FXCollections.observableList(purchases));
    }

    /**
     * Event handler for the <code>showLast10Button</code> event.
     */

    @FXML
    protected void showLast10ButtonClicked() {
        log.debug("Showing last 10 purchases.");
        List<Purchase> purchaseList = dao.findLast10Purchases();
        historyTableView.setItems(FXCollections.observableList(purchaseList));
    }

    /**
     * Event handler for the <code>showBetweenButton</code> event.
     */
    @FXML
    protected void showBetweenButtonClicked() {
        if (startDate.getValue() == null || endDate.getValue() == null) {
            AlertBox.display("Dates not selected", "Please select correct dates.");
            return;
        }
        LocalDate from = startDate.getValue().minusDays(1);
        LocalDate to = endDate.getValue().plusDays(1);
        if (to.isBefore(from)) {
            AlertBox.display("Wrong dates", "End date cannot be before start date.");
            startDate.setValue(null);
            endDate.setValue(null);
            return;
        }

        List<Purchase> suitables = dao.findPurchasesByDate(from, to);

        log.debug("Showing purchases made between " + from + " (including) to " + to + " (including).");
        historyTableView.setItems(FXCollections.observableList(suitables));
    }
}
