package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Encapsulates everything that has to do with the purchase tab (the tab
 * labelled "Point-of-sale" in the menu). Consists of the purchase menu,
 * current purchase dialog and shopping cart table.
 */
public class PurchaseController implements Initializable {

    private static final Logger log = LogManager.getLogger(PurchaseController.class);

    private final SalesSystemDAO dao;
    private final ShoppingCart shoppingCart;

    @FXML
    private Button newPurchase;
    @FXML
    private Button submitPurchase;
    @FXML
    private Button cancelPurchase;
    @FXML
    private TextField barCodeField;
    @FXML
    private TextField quantityField;
    @FXML
    private TextField priceField;
    @FXML
    private Button addItemButton;
    @FXML
    private Button deleteButton;
    @FXML
    private TableView<SoldItem> purchaseTableView;
    @FXML
    private ChoiceBox<String> choiceBox;

    public PurchaseController(SalesSystemDAO dao, ShoppingCart shoppingCart) {
        this.dao = dao;
        this.shoppingCart = shoppingCart;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cancelPurchase.setDisable(true);
        submitPurchase.setDisable(true);
        disableProductField(true);

        this.choiceBox.focusedProperty().addListener((arg0, oldPropertyValue, newPropertyValue) -> {
            if (!newPropertyValue)
                fillInputsByName();
        });
        this.barCodeField.focusedProperty().addListener((arg0, oldPropertyValue, newPropertyValue) -> {
            if (!newPropertyValue)
                fillInputsByBarcode();
        });
    }

    /**
     * Event handler for the <code>new purchase</code> event.
     */

    @FXML
    protected void newPurchaseButtonClicked() {
        log.info("New sale process started");
        try {
            enableInputs();
            purchaseTableView.setItems(FXCollections.observableList(shoppingCart.getAll()));
            choiceBox.getItems().clear();
            choiceBox.getItems().add("");
            List<StockItem> items = dao.findStockItems();
            for (StockItem item : items) {
                choiceBox.getItems().add(item.getName());
            }
        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        } catch (Exception e) {
            log.error("UNEXPECTED: " + e.getMessage());
        }
    }

    /**
     * Event handler for the <code>cancel purchase</code> event.
     */
    @FXML
    protected void cancelPurchaseButtonClicked() {
        log.info("Sale cancelled");
        try {
            shoppingCart.cancelCurrentPurchase();
            disableInputs();
            purchaseTableView.refresh();
        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * Event handler for the <code>submit purchase</code> event.
     */
    @FXML
    protected void submitPurchaseButtonClicked() {
        if (shoppingCart.getAll().size() == 0) {
            AlertBox.display("Cart is empty.", "Please fill the cart to submit the purchase.");
            return;
        }
        log.info("Sale complete");
        try {
            log.debug("Contents of the current basket:" + shoppingCart.getAll());
            shoppingCart.submitCurrentPurchase();
            disableInputs();
            purchaseTableView.refresh();
        } catch (Exception e) {
            log.error(e.getMessage());
            AlertBox.display("An unknown error occurred while trying to submit the purchase",
                    "u need to pay $$ to get this solved");
        }
    }


    // switch UI to the state that allows to proceed with the purchase
    private void enableInputs() {
        resetProductField();
        disableProductField(false);
        cancelPurchase.setDisable(false);
        submitPurchase.setDisable(false);
        newPurchase.setDisable(true);
    }

    // switch UI to the state that allows to initiate new purchase
    private void disableInputs() {
        resetProductField();
        cancelPurchase.setDisable(true);
        submitPurchase.setDisable(true);
        newPurchase.setDisable(false);
        disableProductField(true);
    }

    private void fillInputsByName() {
        String choiceBoxSelected = choiceBox.getSelectionModel().getSelectedItem();
        StockItem stockItem = null;
        if (choiceBoxSelected != null) {
            stockItem = getByName(choiceBoxSelected);
        }
        fillInputs(stockItem);
    }

    private void fillInputsByBarcode() {
        StockItem stockItem = null;
        if (!barCodeField.getText().equals("")) {
            stockItem = getByBarcode();
        }
        fillInputs(stockItem);
    }

    private void fillInputs(StockItem stockItem) {
        if (stockItem != null) {
            barCodeField.setText(stockItem.getId().toString());
            priceField.setText(String.valueOf(stockItem.getPrice()));
            choiceBox.setValue(stockItem.getName());
        } else
            resetProductField();
    }

    private StockItem getByBarcode() {
        try {
            long code = Long.parseLong(barCodeField.getText());
            return dao.findStockItem(code);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    private StockItem getByName(String name) {
        return dao.findStockItem(name);
    }

    /**
     * Add new item to the cart.
     */
    @FXML
    public void addItemEventHandler() {
        // add chosen item to the shopping cart.
        StockItem stockItem = getByBarcode();

        if (stockItem != null) {
            try {
                int quantityInCart = Integer.parseInt(quantityField.getText());
                double sumOfItems = stockItem.getPrice()*quantityInCart;
                shoppingCart.addItem(new SoldItem(stockItem, quantityInCart, sumOfItems));
                resetProductField();
            } catch (NumberFormatException nfe) {
                AlertBox.display("Invalid input", "Amount was not numeric");
            } catch (SalesSystemException sse) {
                log.debug(sse.getMessage());
                AlertBox.display("Invalid input", "Could not add item to cart.\n" + sse.getMessage());
            }
            purchaseTableView.refresh();
            refreshShoppingCart();
        }
        else
            AlertBox.display("Item not selected.", "Please select an item to add to cart.");
    }


    /**
     * Sets whether or not the product component is enabled.
     */
    private void disableProductField(boolean disable) {
        this.addItemButton.setDisable(disable);
        this.barCodeField.setDisable(disable);
        this.quantityField.setDisable(disable);
        this.priceField.setDisable(true);
        this.choiceBox.setDisable(disable);
        this.deleteButton.setDisable(disable);
    }

    /**
     * Reset dialog fields.
     */
    private void resetProductField() {
        barCodeField.setText("");
        quantityField.setText("1");
        priceField.setText("");
        choiceBox.setValue("");
    }

    @FXML
    protected void deleteItemButtonClicked() {
        SoldItem selectedItem = purchaseTableView.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            shoppingCart.removeitem(selectedItem);
            refreshShoppingCart();
        } else {
            AlertBox.display("No items selected for deletion.", "Please select items to delete.");
        }
    }

    protected void refreshShoppingCart() {
        purchaseTableView.setItems(FXCollections.observableList(shoppingCart.getAll()));
        purchaseTableView.refresh();
    }
}
