package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class InMemorySalesSystemDAO implements SalesSystemDAO {

    private final List<StockItem> stockItemList;
    private final List<SoldItem> soldItemList;
    private final List<Purchase> purchaseList;
    private int counterSaveStockitem;

    public InMemorySalesSystemDAO() {
        List<StockItem> items = new ArrayList<>();
        items.add(new StockItem(1L, "Lays chips", "Potato chips", 11.0, 5));
        items.add(new StockItem(2L, "Chupa-chups", "Sweets", 8.0, 8));
        items.add(new StockItem(3L, "Frankfurters", "Beer sauseges", 15.0, 12));
        items.add(new StockItem(4L, "Free Beer", "Student's delight", 0.0, 100));
        this.stockItemList = items;
        this.soldItemList = new ArrayList<>();
        this.purchaseList = new ArrayList<>();
        counterSaveStockitem = 0;
    }

    public int getCounterSaveStockitem() {
        return counterSaveStockitem;
    }

    @Override
    public List<Purchase> findPurchasesByDate(LocalDate beginDate, LocalDate endDate) {
        return null;
    }

    @Override
    public List<StockItem> findStockItems() {
        return stockItemList;
    }

    @Override
    public List<Purchase> findPurchases() {
        return purchaseList;
    }

    @Override
    public List<Purchase> findLast10Purchases() {
        if (purchaseList.size() > 10)
            return purchaseList.subList(purchaseList.size()-10, purchaseList.size());
        return purchaseList;
    }

    @Override
    public StockItem findStockItem(long id) {
        for (StockItem item : stockItemList) {
            if (item.getId() == id)
                return item;
        }
        return null;
    }

    @Override
    public StockItem findStockItem(String name) {
        return null;
    }

    @Override
    public void saveSoldItem(SoldItem item) {
        soldItemList.add(item);
    }

    @Override
    public void saveStockItem(StockItem stockItem) {
        counterSaveStockitem++;
        stockItemList.add(stockItem);
    }

    @Override
    public void removeStockItem(StockItem stockItem) {
        Iterator<StockItem> iterator = stockItemList.iterator();
        while (iterator.hasNext()) {
            Long id = iterator.next().getId();
            if (id.equals(stockItem.getId())) {
                iterator.remove();
                break;
            }
        }
    }

    @Override
    public void beginTransaction() {
    }

    @Override
    public void rollbackTransaction() {
    }

    @Override
    public void commitTransaction() {
    }

    @Override
    public void savePurchase(Purchase purchase) {
        purchaseList.add(purchase);
    }
}
