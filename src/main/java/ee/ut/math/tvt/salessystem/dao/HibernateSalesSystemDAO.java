package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.time.LocalDate;
import java.util.List;

public class HibernateSalesSystemDAO implements SalesSystemDAO {

    private final EntityManagerFactory emf;
    private final EntityManager em;

    public HibernateSalesSystemDAO() {
        // if you get ConnectException/JDBCConnectionException then you
        // probably forgot to start the database before starting the application
        emf = Persistence.createEntityManagerFactory("pos");
        em = emf.createEntityManager();
    }

    public void close() {
        em.close();
        emf.close();
    }

    @Override
    public List<StockItem> findStockItems() {
        return em.createQuery("from StockItem", StockItem.class).getResultList();
    }

    @Override
    public List<Purchase> findPurchases() {
        return em.createQuery("from Purchase", Purchase.class).getResultList();
    }

    @Override
    public List<Purchase> findLast10Purchases() {
        return em.createQuery("from Purchase order by id desc", Purchase.class).setMaxResults(10).getResultList();
    }

    @Override
    public List<Purchase> findPurchasesByDate(LocalDate beginDate, LocalDate endDate) {
        return em.createQuery("FROM Purchase WHERE date BETWEEN :beginDate AND :endDate")
                .setParameter("beginDate", beginDate)
                .setParameter("endDate", endDate)
                .getResultList();
    }

    @Override
    public StockItem findStockItem(long id) {
        return em.find(StockItem.class, id);
    }

    @Override
    public StockItem findStockItem(String name) {
        return (StockItem) em.createQuery(
                "SELECT c FROM StockItem c WHERE c.name LIKE :itemName")
                .setParameter("itemName", name)
                .getSingleResult();
    }

    @Override
    public void saveSoldItem(SoldItem item) {
        em.persist(item);
    }

    @Override
    public void saveStockItem(StockItem stockItem) {
        em.persist(stockItem);
    }

    @Override
    public void removeStockItem(StockItem stockItem) {
        em.remove(stockItem);
    }

    @Override
    public void beginTransaction() {
        em.getTransaction().begin();
    }

    @Override
    public void rollbackTransaction() {
        em.getTransaction().rollback();
    }

    @Override
    public void commitTransaction() {
        em.getTransaction().commit();
    }

    @Override
    public void savePurchase(Purchase purchase) {
        em.persist(purchase);
    }

    @Override
    public int getCounterSaveStockitem() {
        return 0;
    }
}