package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class Warehouse {
    private static final Logger log = LogManager.getLogger(ShoppingCart.class);
    private final SalesSystemDAO dao;

    public Warehouse(SalesSystemDAO dao) {
        this.dao = dao;
    }


    public StockItem deleteItemFromWarehouse(StockItem selectedItem) {
        dao.removeStockItem(selectedItem);
        return selectedItem;
    }


    public void createItemAndAddToWarehouse(String barcodeStr, String nameStr, String priceStr, String quantityStr) {
        long barcode = checkBarcode(barcodeStr);
        double price = checkPrice(priceStr);
        int quantity = checkQuantity(quantityStr);

        if (nameStr.equals(""))
            throw new SalesSystemException("Item name was not entered.");
        if (barcode <= 0 || price < 0 || quantity <= 0)
            throw new SalesSystemException("Non-positive values from input fields");

        StockItem newItem = new StockItem(barcode, nameStr, "unique description for all :)", price, quantity);
        addNewItemToDao(newItem);
    }

    private void addNewItemToDao(StockItem newItem) {
        List<StockItem> items = dao.findStockItems();
        if (items != null) {
            for (StockItem item : items) {
                boolean sameNameAndPrice = item.getName().equalsIgnoreCase(newItem.getName()) && item.getPrice() == newItem.getPrice();
                if (item.getId().equals(newItem.getId()) && !sameNameAndPrice)
                    throw new SalesSystemException("This barcode already exists in database.");
                if (sameNameAndPrice) {
                    item.setQuantity(item.getQuantity() + newItem.getQuantity());
                    log.debug("Increased the quantity of " + item.toString());
                    return;
                }
                if (item.getName().equalsIgnoreCase(newItem.getName()))
                    throw new SalesSystemException("An item with this name already exists in the database.");
            }
        }

        dao.beginTransaction();
        try {
            dao.saveStockItem(newItem);
            log.debug("Added item " + newItem.toString() + " to the database.");
            dao.commitTransaction();
        } catch (Exception e) {
            log.debug("Transaction failed, rollback. Message: " + e.getMessage());
            dao.rollbackTransaction();
            throw e;
        }
    }

    private int checkQuantity(String quantityStr) {
        try {
            return Integer.parseInt(quantityStr);
        } catch (NumberFormatException nfe) {
            throw new SalesSystemException("Quantity was incorrect");
        }
    }

    private double checkPrice(String priceStr) {
        try {
            return Double.parseDouble(priceStr);
        } catch (NumberFormatException nfe) {
            throw new SalesSystemException("Price was incorrect");
        }
    }

    private long checkBarcode(String barcodeStr) {
        if (barcodeStr.equals("")) {
            long idOfLastItemInDAO;
            List<StockItem> items = dao.findStockItems();
            if (items != null && !items.isEmpty()) {
                idOfLastItemInDAO = items.get(items.size() - 1).getId();
            } else
                idOfLastItemInDAO = 0;
            return idOfLastItemInDAO + 1;
        }
        try {
            return Long.parseLong(barcodeStr);
        } catch (NumberFormatException nfe) {
            throw new SalesSystemException("Barcode was incorrect");
        }
    }
}
