package ee.ut.math.tvt.salessystem.dataobjects;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Purchase")
public class Purchase {

    @Id
    @GeneratedValue
    private Long id;
    @OneToMany
    private final List<SoldItem> items = new ArrayList<>();
    private LocalDate date;
    private String time;
    private Double total;

    public Purchase(List<SoldItem> soldItems) {
        this.date = LocalDate.now();
        this.time = LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss:00.00"));
        this.items.addAll(soldItems);
        this.total = sumUpItems(soldItems);
    }

    public Purchase() {

    }

    private static Double sumUpItems(List<SoldItem> items) {
        double sumOfItems = 0;
        for (SoldItem item : items)
            sumOfItems += item.getPrice() * item.getQuantity();
        return sumOfItems;
    }

    public List<SoldItem> getItems() {
        return items;
    }

    public LocalDate getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public  Double getTotal() {
        return total;
    }

    public String toString(){
        return time + " cart contents: " + items.toString();
    }
}
