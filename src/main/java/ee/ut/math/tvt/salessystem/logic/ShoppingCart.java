package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;


public class ShoppingCart {

    private static final Logger log = LogManager.getLogger(ShoppingCart.class);
    private final SalesSystemDAO dao;
    private final List<SoldItem> items = new ArrayList<>();
    private final List<String> transactionSteps = new ArrayList<>(); // for testing purposes. I think it would be better to use Mockito, but don't have time to learn that atm.

    public ShoppingCart(SalesSystemDAO dao) {
        this.dao = dao;
    }

    public List<String> getTransactionSteps() {
        return transactionSteps;
    }

    /**
     * Add new SoldItem to table.
     */
    public void addItem(SoldItem item) {

        if (item.getQuantity() <= 0)
            throw new SalesSystemException("Entered amount was not positive.");
        int quantityInWarehouse = 0;
        quantityInWarehouse += dao.findStockItem(item.getStockId()).getQuantity();

        for (SoldItem itemInCart : items) {
            if (itemInCart.equals(item)) {
                int newQuantity = itemInCart.getQuantity() + item.getQuantity();
                if (newQuantity > quantityInWarehouse)
                    throw new SalesSystemException("Don't have enough items in the warehouse.");
                log.debug("User increased the quantity of " + itemInCart.getName() + " in the cart.");
                itemInCart.setQuantity(newQuantity);
                itemInCart.setSumOfItems(itemInCart.getSumOfItems() + item.getSumOfItems());
                return;
            }
        }
        if (item.getQuantity() > quantityInWarehouse) {
            throw new SalesSystemException("Don't have enough items in the warehouse.");
        }
        items.add(item);
        log.debug("User added " + item.toString() + " to the cart");
    }

    public void removeitem(SoldItem item) {
        items.remove(item);
    }

    public List<SoldItem> getAll() {
        return items;
    }

    public void cancelCurrentPurchase() {
        items.clear();
    }

    public void submitCurrentPurchase() {
        transactionSteps.clear();

        // note the use of transactions. InMemorySalesSystemDAO ignores transactions
        // but when you start using hibernate in lab5, then it will become relevant.
        // what is a transaction? https://stackoverflow.com/q/974596
        dao.beginTransaction();
        transactionSteps.add("begin");
        try {
            dao.savePurchase(new Purchase(items));
            for (SoldItem item : items) {
                dao.saveSoldItem(item);
                updateWarehouse(item);
            }

            dao.commitTransaction();
            transactionSteps.add("commit");
            items.clear();
        } catch (Exception e) {
            log.debug("Transaction failed, rollback. Message: " + e.getMessage());
            dao.rollbackTransaction();
            throw e;
        }
    }

    private void updateWarehouse(SoldItem item) {
        List<StockItem> items = dao.findStockItems();
        if (items == null)
            return;

        for (StockItem stockitem : items) // should query for a specific item
            if (stockitem.getName().equals(item.getName()) && stockitem.getPrice()==item.getPrice()) {
                int newQuantity = stockitem.getQuantity() - item.getQuantity();
                stockitem.setQuantity(newQuantity);
                break;
            }
    }
}
