package ee.ut.math.tvt.salessystem.dataobjects;


import javax.persistence.*;

/**
 * Already bought StockItem. SoldItem duplicates name and price for preserving history.
 */
@Entity
@Table(name = "SoldItem")
public class SoldItem {

    @Id
    @GeneratedValue
    private Long id;
    private Long stockId;
    private String name;
    private Integer quantity;
    private double price;
    private double sumOfItems;

    public SoldItem() {
    }

    public SoldItem(StockItem stockItem, int quantity, double sumOfItems) {
        this.name = stockItem.getName();
        this.price = stockItem.getPrice();
        this.quantity = quantity;
        this.sumOfItems = sumOfItems;
        this.stockId = stockItem.getId();
    }

    public Long getId() {
        return id;
    }

    public long getStockId() {
        return stockId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public double getSumOfItems() {
        return sumOfItems;
    }

    public void setSumOfItems(double sumOfItems) {
        this.sumOfItems = sumOfItems;
    }


    @Override
    public String toString() {
        return String.format("SoldItem{id=%d, name='%s'}", id, name);
    }

    public boolean equals(SoldItem so) {
        return (this.getName() == so.getName() && this.getPrice()==so.getPrice());
    }
}
