package ee.ut.math.tvt.salessystem.logic;
import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.mockito.Mockito.*;

import static org.junit.Assert.assertEquals;

public class WarehouseTest {

    private InMemorySalesSystemDAO dao;
    private Warehouse warehouse;

    @Before
    public void setUp() {
        dao = new InMemorySalesSystemDAO();
        warehouse = new Warehouse(dao);
    }

    @Test
    public void testAddingNewItem(){
        int counterSaveStockitem = dao.getCounterSaveStockitem();
        warehouse.createItemAndAddToWarehouse("777", "Bread", "2", "55");
        List<StockItem> items = dao.findStockItems();
        long lastElementsId = items.get(items.size() - 1).getId();
        int counterAfterAdding = dao.getCounterSaveStockitem();
        assertEquals(777L, lastElementsId);
        assertEquals(counterSaveStockitem + 1, counterAfterAdding);
    }

    @Test // duplication of previous test, but I'll leave it in to remind myself later how Mockito is used
    public void testAddingNewItem2(){
        SalesSystemDAO mockDAO = mock(InMemorySalesSystemDAO.class);
        Warehouse mockWarehouse = new Warehouse(mockDAO);
        mockWarehouse.createItemAndAddToWarehouse("101", "Sausage", "10", "5");
        verify(mockDAO, times(1)).saveStockItem(any());
    }



    @Test
    public void testAddingExistingItem() {
        // add item and check if counter is incremented by one
        int counterSaveStockitem = dao.getCounterSaveStockitem();
        warehouse.createItemAndAddToWarehouse("246", "Butter", "1", "10");
        int counterAfterAdding = dao.getCounterSaveStockitem();
        assertEquals(counterSaveStockitem + 1, counterAfterAdding);

        // add item and check that counter is not incremented
        counterSaveStockitem = dao.getCounterSaveStockitem();
        warehouse.createItemAndAddToWarehouse("246", "Butter", "1", "115");
        counterAfterAdding = dao.getCounterSaveStockitem();
        assertEquals(counterSaveStockitem , counterAfterAdding);

        // test that quantity is increased (instead of adding new item)
        int quantity = dao.findStockItem(246L).getQuantity();
        assertEquals(125, quantity);
    }

    @Test // POINTLESS TEST
    public void testAddingExistingItem2(){
        InMemorySalesSystemDAO dao2 = mock(InMemorySalesSystemDAO.class);
        Warehouse mockWarehouse = new Warehouse(dao2);
        mockWarehouse.createItemAndAddToWarehouse("15", "Butter", "1", "10");
        mockWarehouse.createItemAndAddToWarehouse("15", "Butter", "1", "115");

        // I left this one here as it is, so I could dive into this sometime in the future.
        // I suspect that Mockito just mocks the functionality, but does not remember what was done before (added 10 butters to the warehouse).
        // So the next time mocked dao is called, it does not know that we already have butter.

        verify(dao2, times(2)).saveStockItem(any());
    }

    @Test(expected = SalesSystemException.class)
    public void testAddingItemWithNegativeQuantity(){
        warehouse.createItemAndAddToWarehouse("25", "Tomato", "2", "-10");
    }
}
