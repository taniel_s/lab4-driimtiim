package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.HibernateSalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ShoppingCartTest {
    private ShoppingCart cart;
    private SalesSystemDAO dao;

    @Before
    public void setUp() {
        this.dao = new InMemorySalesSystemDAO();
        this.cart = new ShoppingCart(dao);
    }

    @Test
    public void testAddingItemBeginsAndCommitsTransaction() {
        assert cart.getAll().size() == 0;  // make sure that cart is empty
        fillCartWith10RandomItems();
        assert cart.getAll().size() == 10; // 10 items added
        assert cart.getTransactionSteps().size() == 0; // no DB interaction
        cart.submitCurrentPurchase();
        assert cart.getAll().size() == 0; // make sure that cart is emptied
        assert cart.getTransactionSteps().get(0).equals("begin");
        assert cart.getTransactionSteps().get(1).equals("commit");
        assert cart.getTransactionSteps().size() == 2;
        clearUp();
    }

    @Test
    public void testIfPurchaseIsSavedToDAO() {
        assert dao.findPurchases().size() == 0; // no purchases in DB
        fillCartWith10RandomItems();
        cart.submitCurrentPurchase();
        assert dao.findPurchases().size() == 1;
        clearUp();
    }

    @Test
    public void testAddingExistingItem() {
        SoldItem testItem = new SoldItem(dao.findStockItem(1), 1, 2);
        cart.addItem(testItem);
        int quantityBeforeAddition = testItem.getQuantity();
        cart.addItem(testItem);
        int quantityAfterAddition = testItem.getQuantity();
        assert quantityAfterAddition > quantityBeforeAddition;
        clearUp();
    }

    @Test
    public void testAddingNewItem() {
        int cartSize = cart.getAll().size();
        StockItem stockItem = dao.findStockItem(1);
        SoldItem soldItem = new SoldItem(stockItem, 1, 2);
        cart.addItem(soldItem);
        assert cart.getAll().get(cartSize).equals(soldItem);
        clearUp();
    }

    @Test(expected = SalesSystemException.class)
    public void testAddingItemWithNegativeQuantityThrowsSSE() {
        StockItem stockItem = new StockItem(0L, "name", "descr", 10, 10);
        SoldItem soldItem = new SoldItem(stockItem, -1, stockItem.getPrice());
        cart.addItem(soldItem); // throws SSE
        clearUp();
    }

    @Test(expected = SalesSystemException.class)
    public void testAddingItemWithQuantityTooLargeThrowsSSE() {
        StockItem tester = new StockItem(888888888L, "Eit", "", 8.8, 8);
        dao.saveStockItem(tester);
        SoldItem soldItem = new SoldItem(tester, tester.getQuantity()+1, 99);
        cart.addItem(soldItem); // throws SSE
        clearUp();
    }

    @Test
    public void testAddingItemWithQuantitySumTooLarge() {
        StockItem tester = new StockItem(888888888L, "Eit", "", 8.8, 8);
        dao.saveStockItem(tester);
        // Make sure adding too much throws error straight away.
        Assert.assertThrows(SalesSystemException.class, () -> cart.addItem(new SoldItem(tester, 9, 99)));
        // Add max possible amount without errors.
        cart.addItem(new SoldItem(tester, 8, 99));
        // Add 1 more and assert that ex is thrown.
        Assert.assertThrows(SalesSystemException.class, () -> {
            cart.addItem(new SoldItem(tester, 1, 99));
        });
        clearUp();
    }

    public void fillCartWith10RandomItems() {
        for (long i = 0; i < 10; i++) { // adding 10 random items
            Random r = new Random();
            StockItem item = new StockItem(i, "item" + i, "", r.nextDouble(), r.nextInt());
            int quantity = Math.abs(r.nextInt());
            SoldItem sold = new SoldItem(item, quantity, quantity * item.getPrice());
            cart.getAll().add(sold);

            // Java 101... I don't understand why this does not work..
            // cart.addItem(sold);
        }
    }

    private void clearUp() {
        dao.findPurchases().clear();
        dao.findStockItems().clear();
        cart.getAll().clear();
        cart.getTransactionSteps().clear();
    }

    @Test
    public void testSubmittingCurrentPurchaseDecreasesStockItemQuantity() {
        // Add 100 potatoes to warehouse and 77 of them to cart
        StockItem tester = new StockItem(7777777777L, "Potato", "a bit too soft, but OK", 3.83, 100);
        dao.saveStockItem(tester);
        cart.addItem(new SoldItem(tester, 77, 3.83*77));
        // Verify that WH has 100 potatoes
        int quantityInWH = dao.findStockItem(7777777777L).getQuantity();
        assert quantityInWH == 100;
        // Submit and verify
        cart.submitCurrentPurchase();
        quantityInWH = dao.findStockItem(7777777777L).getQuantity();
        assert quantityInWH == 23;
        clearUp();
    }

    @Test
    public void testSubmittingCurrentOrderCreatesPurchaseItem() {
        fillCartWith10RandomItems();
        List<SoldItem> cartContents = new ArrayList<>(cart.getAll());
        cart.submitCurrentPurchase();
        assert dao.findPurchases().size() == 1; // Purchase item is created.
        List<SoldItem> purchasedItems = dao.findPurchases().get(0).getItems();
        // assert all items are same
        for (int i = 0; i < 10; i++) {
            assert cartContents.get(i).equals(purchasedItems.get(i));
        }
    }

    @Test
    public void testSubmittingCurrentOrderSavesCorrectTime() {
        fillCartWith10RandomItems();
        String now = LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss:00.00"));
        cart.submitCurrentPurchase();
        String purchaseTime = dao.findPurchases().get(0).getTime();
        // I think it may throw fails from time to time.
        // Should modify to accept maybe 1 sec time differences, but it's not a real world app
        assert now.equals(purchaseTime);
        clearUp();
    }

    @Test
    public void testCancellingOrder() {
        assert cart.getAll().size() == 0;
        fillCartWith10RandomItems();
        assert cart.getAll().size() == 10;
        cart.cancelCurrentPurchase();
        assert cart.getAll().size() == 0;
    }

    @Test
    public void testCancellingOrderQuantitiesUnchanged() {
        StockItem tester = new StockItem(7777777777L, "Potato", "a bit too soft, but OK", 3.83, 100);
        dao.saveStockItem(tester);
        assert dao.findStockItem(7777777777L).getQuantity() == 100;
        // Put some to cart, verify them being there
        cart.addItem(new SoldItem(tester, 77, 3.83*77));
        assert cart.getAll().get(0).getStockId() == 7777777777L;
        // cancel and verify that warehouse was not modified
        cart.cancelCurrentPurchase();
        assert dao.findStockItem(7777777777L).getQuantity() == 100;
        clearUp();
    }
}
