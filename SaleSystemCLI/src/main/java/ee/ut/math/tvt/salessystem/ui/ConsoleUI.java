package ee.ut.math.tvt.salessystem.ui;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.HibernateSalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import ee.ut.math.tvt.salessystem.logic.Warehouse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.*;

/**
 * A simple CLI (limited functionality).
 */
public class ConsoleUI {
    private static final Logger log = LogManager.getLogger(ConsoleUI.class);

    private final SalesSystemDAO dao;
    private final ShoppingCart cart;
    private final Warehouse warehouse;

    public ConsoleUI(SalesSystemDAO dao) {
        this.dao = dao;
        cart = new ShoppingCart(dao);
        warehouse = new Warehouse(dao);
    }

    public static void main(String[] args) throws Exception {
        SalesSystemDAO dao = new HibernateSalesSystemDAO();
        ConsoleUI console = new ConsoleUI(dao);
        console.run();
    }

    /**
     * Run the sales system CLI.
     */
    public void run() throws IOException {
        System.out.println("===========================");
        System.out.println("=       Sales System      =");
        System.out.println("===========================");
        printUsage();
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.print("> ");
            processCommand(in.readLine().trim().toLowerCase());
            System.out.println("Done. ");
        }
    }

    private void showStock() {
        List<StockItem> stockItems = dao.findStockItems();
        System.out.println("-------------------------");
        for (StockItem si : stockItems) {
            String inCart = inCart(si);
            System.out.println(si.getId() + " " + si.getName() + " " + si.getPrice() + "Euro (" + si.getQuantity() + " items" + inCart + ")");
        }
        if (stockItems.size() == 0) {
            System.out.println("\tNothing");
        }
        System.out.println("-------------------------");
    }

    private String inCart(StockItem si) {
        for (SoldItem soldItem : cart.getAll()) {
            if (soldItem.getName().equals(si.getName()) && soldItem.getPrice()==si.getPrice())
                return ", of which " + soldItem.getQuantity() + " already in cart";
        }
        return "";
    }

    private void showCart() {
        System.out.println("-------------------------");
        for (SoldItem si : cart.getAll()) {
            System.out.println(si.getName() + " " + si.getPrice() + "Euro (" + si.getQuantity() + " items)");
        }
        if (cart.getAll().size() == 0) {
            System.out.println("\tNothing");
        }
        System.out.println("-------------------------");
    }

    private void showTeam() {

        System.out.println("===========================");
        System.out.println("=         Team info       =");
        System.out.println("===========================");

        try (InputStream input = new FileInputStream("..\\src\\main\\resources\\application.properties")) {
            Properties prop = new Properties();
            prop.load(input);
            System.out.println("Team name" + " ".repeat(30 - ("Team name").length()) + prop.getProperty("teamName"));
            System.out.println("Team leader" + " ".repeat(30 - ("Team leader").length()) + prop.getProperty("teamLeader"));
            System.out.println("Team leader email" + " ".repeat(30 - ("Team leader email").length()) + prop.getProperty("teamLeaderEmail"));
            System.out.println("Team members" + " ".repeat(30 - ("Team members").length()) + prop.getProperty("teamMembers"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void showHistory() {
        System.out.println("Press t to see last 10 purchases, anything else to see them all.");
        String input = new Scanner(System.in).nextLine();
        List<Purchase> purchases = (input.equals("t")) ? dao.findLast10Purchases() : dao.findPurchases();

        listAllPurchases(purchases);
        if (purchases.size() != 0)
            System.out.print("\nEnter purchase ID for detailed purchase info, enter n if you want to skip.\n>");
        else System.out.println("No purchases in database.");

        input = new Scanner(System.in).nextLine();
        if (!input.equals("n")) {
            displayPurchase(purchases, input);
        }
    }

    private void displayPurchase(List<Purchase> purchases, String cmd) {
        int id;
        Purchase p;
        try {
            id = Integer.parseInt(cmd);
            p = purchases.get(id);
        } catch (NumberFormatException | IndexOutOfBoundsException e) {
            System.out.println("Please enter correct index.");
            return;
        }
        if (p != null) {
            System.out.println("Items sold during this purchase:");
            for (SoldItem item : p.getItems()) {
                System.out.println("Item ID: " + item.getId() +
                        " Name: " + item.getName() +
                        " Price: " + item.getPrice() +
                        " Quantity: " + item.getQuantity() +
                        " Total: " + item.getSumOfItems());
            }
        } else
            System.out.println("Please enter correct index.");
    }

    private void listAllPurchases(List<Purchase> purchases) {
        for (Purchase purchase : purchases) {
            System.out.println("ID: " + purchases.indexOf(purchase) +
                            " Date: " + purchase.getDate() +
                            " Time: " + purchase.getTime() +
                            " Total: " + purchase.getTotal());
        }
    }

    private void printUsage() {
        System.out.println("-------------------------");
        System.out.println("Usage:");
        System.out.println("h\t\tShow this help");
        System.out.println("w\t\tShow warehouse contents");
        System.out.println("n IDX NAME PR NR \tAdd NR of stock item with index IDX, NAME and price PR to the warehouse.");
        System.out.println("\t\t\t\t\tIf name contains spaces, please use underscore instead (Lays chips -> Lays_chips).");
        System.out.println("\t\t\t\t\tIf you want the barcode to be automatically generated, mark IDX as 'x'.");
        System.out.println("\t\t\t\t\tExample: n x free_beer 0 1000");
        System.out.println("c\t\tShow cart contents");
        System.out.println("a IDX NR \tAdd NR of stock item with index IDX to the cart");
        System.out.println("d IDX \tDelete item with index IDX from the cart");
        System.out.println("dw IDX \tDelete item with index IDX from warehouse");
        System.out.println("p\t\tPurchase the shopping cart");
        System.out.println("r\t\tReset the shopping cart");
        System.out.println("t\t\tShow team view");
        System.out.println("s\t\tShow history view");
        System.out.println("q\t\tExit");
        System.out.println("-------------------------");
    }

    private void processCommand(String command) {
        String[] c = command.split(" ");

        if (c[0].equals("h"))
            printUsage();
        else if (c[0].equals("q"))
            System.exit(0);
        else if (c[0].equals("w"))
            showStock();
        else if (c[0].equals("n") && c.length == 5) {
            if (c[1].equals("x")) c[1] = "";
            c[2] = c[2].replace('_', ' ');
            try {
                warehouse.createItemAndAddToWarehouse(c[1], c[2], c[3], c[4]);
            } catch (SalesSystemException sse) {
                log.debug("Invalid input: " + sse.getMessage());
            }
        } else if (c[0].equals("c"))
            showCart();
        else if (c[0].equals("p"))
            cart.submitCurrentPurchase();
        else if (c[0].equals("r"))
            cart.cancelCurrentPurchase();
        else if (c[0].equals("a") && c.length == 3) {
            addToCart(c);
        } else if (c[0].equals("t"))
            showTeam();
        else if (c[0].equals("s"))
            showHistory();
        else if (c[0].equals("d")) {
            deleteFromCart(c);
        } else if (c[0].equals("dw")) {
            deleteFromWarehouse(c);
        } else {
            System.out.println("unknown command");
        }
    }

    private void addToCart(String[] c) {
        try {
            long idx = Long.parseLong(c[1]);
            int amount = Integer.parseInt(c[2]);
            StockItem item = dao.findStockItem(idx);
            if (item != null) {
                if (amount > item.getQuantity()) {
                    amount = item.getQuantity();
                    System.out.println("======== Could add only " + amount + " items to cart, warehouse out of stock ==========");
                }
                double sum = item.getPrice()*amount;
                cart.addItem(new SoldItem(item, amount, sum));
            } else {
                System.out.println("no stock item with id " + idx);
            }
        } catch (SalesSystemException | NoSuchElementException e) {
            log.error(e.getMessage(), e);
        }
    }

    private void deleteFromWarehouse(String[] c) {
        try {
            long id = Long.parseLong(c[1]);
            StockItem item = dao.findStockItem(id);
            if (item != null) dao.removeStockItem(item);
            else System.out.println("No such item in warehouse");
        } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
            System.out.println("Please enter command as instructed.");
        }
    }

    private void deleteFromCart(String[] c) {
        try {
            long id = Long.parseLong(c[1]);
            boolean itemInCart = false;
            for (SoldItem item : cart.getAll()) {
                if (item.getId().equals(id)) {
                    cart.removeitem(item);
                    itemInCart = true;
                    break;
                }
            }
            if (!itemInCart) System.out.println("No such item in cart");
        } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
            System.out.println("Please enter command as instructed.");
        }
    }

}
