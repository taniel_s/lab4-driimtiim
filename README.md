# Team <driimtiim>:
1. Silver Kolde
2. Taniel Saarevet
3. Gustav Nikopensius

## Homework 1:
https://bitbucket.org/taniel_s/lab4-driimtiim/wiki/Homework%201

## Homework 2:
https://bitbucket.org/taniel_s/lab4-driimtiim/wiki/Homework%202

## Homework 3:
https://bitbucket.org/taniel_s/lab4-driimtiim/commits/tag/homework-3

## Homework 4:
https://bitbucket.org/taniel_s/lab4-driimtiim/wiki/howemork

## Homework 5:
https://bitbucket.org/taniel_s/lab4-driimtiim/commits/tag/homework-5

## Homework 6:
https://docs.google.com/spreadsheets/d/1YMwW7Y_U9vJOyhIaIrZWOhewaK-sJW1lSNpKxlXaOWA/edit?usp=sharing

https://bitbucket.org/taniel_s/lab4-driimtiim/wiki/5%20test%20cases

https://bitbucket.org/taniel_s/lab4-driimtiim/commits/tag/homework-6

## Homework 7:
Usability tests:
https://bitbucket.org/taniel_s/lab4-driimtiim/wiki/Lab%207,%20Usability%20tests

zip-file:
https://drive.google.com/file/d/1mKhUiIhieR0U8jQRbpGK6l2JjZv78Oty/view?usp=sharing

Functional tests:
https://bitbucket.org/taniel_s/lab4-driimtiim/wiki/Task%202.1

Task 2.2: 
https://bitbucket.org/taniel_s/lab4-driimtiim/wiki/Task%202.2


We encourage you to use [markdown syntax](https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)